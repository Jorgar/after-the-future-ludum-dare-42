﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LDSubjectManager : MonoBehaviour {

    public Animator animator;

    public void SubjectOut(){
        animator.SetTrigger("subject_out");
    }

    public void SubjectBurn()
    {
        animator.SetTrigger("subject_burn");
    }
}
