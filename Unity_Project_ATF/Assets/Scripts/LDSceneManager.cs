﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LDSceneManager : MonoBehaviour {



    public void PlayGame()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        Constants.isNewPlayer = true;
    }

}
