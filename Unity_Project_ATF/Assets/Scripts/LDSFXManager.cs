﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LDSFXManager : MonoBehaviour {

    public AudioSource typingSound;
    public AudioSource alertSound;
    public AudioSource powerOff;
    public AudioSource subjectScream;
	

    public void RobotTyping(bool hasToType){
        if(hasToType){
            typingSound.Play();
        }else{
            typingSound.Stop();
        }
    }

    public void AlertSound()
    {
        alertSound.Play();
    }

    public void RobotPower()
    {
        powerOff.Play();
    }

    public void SubjectScream()
    {
        subjectScream.Play();
    }
}
