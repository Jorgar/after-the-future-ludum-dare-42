﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LDGameManager : MonoBehaviour {

    //propieties
    public LDSFXManager soundManager;

    public GameObject canvas;
    public GameObject subject;
    public GameObject robot;
    private LDSubjectManager subjectManager;
    private LDRobotManager robotManager;

    public SpriteRenderer[] cellsSubjectList = new SpriteRenderer[10];
    public Image[] reportCellsSubjectResult = new Image[10];
    public GameObject canvasWindowsReport;
    public TextMeshProUGUI calification;
    public GameObject pauseCanvas;
    public GameObject canvasTutorial;

    public int life = 0;
    public int space = 50;
    public float time = 30;
    public int power = 100;
    public int[] testSubjects = new int[10]; //1 success; 0 failed;

    public Text lifeText;
    public Text spaceText;
    public Text timeText;
    public Text powerText;
    public TextMeshProUGUI key1, key2, key3;
    public Button buttonThunder, buttonBattery, buttonBin;
    public GameObject feedbackMessagePrefab;
    public TextMeshProUGUI subjectName;

    private int subjectCounter = 0;
    private float lifeTimer = 0;
    private float powerTimer = 0;
    private float mimicChallengeTimer = 0;
    private KeyCode[] actualKeyChallenge = new KeyCode[3];
    private int mimicPosition = 0;
    private int positionActualSubject = 0;
    private bool mimicPaused = false;
    private bool hasOneThunder = false, hasOneBattery = false, hasOneBin = false;
    private bool isPower = true;
    private bool isTimeUIDescending = false;
    private bool isRobotShutDown = false;
    private string dicctionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    //design variables
    public int periodLifeIncrease = 5;
    public int lifeUnitsToIncrease = 5;
    public int periodPowerDecrase = 20;
    public int powerUnitsToDecrease = 10;
    public int secondsPerMimicChallenge = 3;
    public int secondsToRecharge = 10;
    public float rechargeTimer = 0;
    private bool pauseMechanichs = false;

    //UI effects
    public GameObject timeMask;
    public GameObject spaceMask;
    public GameObject lifeMask;

    public GameObject robotBatteryMask;
    private bool isBatteryDescending = false;

    public SpriteRenderer lifeCells;

    public SpriteRenderer boxLetter1, boxLetter2, boxLetter3;
    public SpriteRenderer linesScreen;

    public Color iconDeactivated;
    public Color greenColor;
    public Color redColor;

    public GameObject screenAlert;

	private void Start()
	{
        Time.timeScale = 1;
        if (Constants.isNewPlayer)
            OpenTutorial();
        SetUpMiMicSystem();
        subjectManager = subject.GetComponent<LDSubjectManager>();
        robotManager = robot.GetComponent<LDRobotManager>();
        subjectName.text = "SUBJECT " + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "" + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "-" + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "" + (char)dicctionary[Random.Range(0, dicctionary.Length)];
	}

	// Update is called once per frame
	void Update () {
        if (!pauseMechanichs)
        {
            RunningTime();
            UpdateLife();
            SpaceUpdate();
            UpdatePower();
            MimicSystemUpdate();
            UpdateButtons();
            UpdateGameState();
        }

        if(Input.GetKeyDown(KeyCode.Escape)){
            Time.timeScale = 0;
            pauseCanvas.SetActive(true);
        }
	}

    void UpdateGameState()
    {
        if (positionActualSubject == 10){
            pauseMechanichs = true;
            OpenReport();
        }
    }

    void RunningTime(){
        if((int)time <= 0){
            testSubjects[positionActualSubject] = 0;
            cellsSubjectList[positionActualSubject].color = redColor; //added to cell color
            positionActualSubject++;
            subjectManager.SubjectBurn();
            soundManager.SubjectScream();

            if (positionActualSubject > testSubjects.Length)
            {
                EndRound();
            }
            else
            {
                NextSubject(true);
            }
        }
        time -= Time.deltaTime;
        timeText.text = "seconds: " + (int)time;
        if((int)time % 2 == 0 && !isTimeUIDescending){
            StartCoroutine(TimeDiscountUI());

        }
    }

    IEnumerator TimeDiscountUI(){
        isTimeUIDescending = true;
        timeMask.GetComponent<Transform>().localScale = new Vector3(timeMask.GetComponent<Transform>().localScale.x - 0.50f,
                                                                        timeMask.GetComponent<Transform>().localScale.y,
                                                                        timeMask.GetComponent<Transform>().localScale.z);
        yield return new WaitForSeconds(1);
        isTimeUIDescending = false;

    }

    void UpdateSpaceUI(){
        spaceMask.GetComponent<Transform>().localScale = new Vector3(space/10,
                                                                        timeMask.GetComponent<Transform>().localScale.y,
                                                                        timeMask.GetComponent<Transform>().localScale.z);
    }

    void UpdateLife(){
        if(life == 100){
            testSubjects[positionActualSubject] = 1;
            cellsSubjectList[positionActualSubject].color = greenColor; //added to cell color
            positionActualSubject++;
            space = Mathf.Clamp(space + 20, 0, 100);
            DisplayMessage(spaceText.gameObject, "+20", Color.red);

            if(positionActualSubject > testSubjects.Length){
                EndRound();
            }else{
                NextSubject(false);
            }
        }
        lifeText.text = "CLONE: " + life.ToString();

        /* 
         * I dont think is necessary to help the
         * player with an small autoincrement
         * 
        lifeTimer += Time.deltaTime;
        if((int)lifeTimer == periodLifeIncrease){
            life += lifeUnitsToIncrease;
            lifeTimer = 0;
            GameObject message = Instantiate(feedbackMessagePrefab, lifeText.transform.position, Quaternion.identity);
            Destroy(message, 2);
        }
        */
    }

    void UpdatePower(){
        powerText.text = "power: " + power;
        if(isPower){
            powerTimer += Time.deltaTime;
            if((int)powerTimer == periodPowerDecrase){
                power -= powerUnitsToDecrease;
                powerTimer = 0;
                DisplayMessage(powerText.gameObject, "-" + powerUnitsToDecrease, Color.red);
            }
        }

        if(power == 0){
            isPower = false;

            buttonThunder.interactable = false;
            buttonBattery.interactable = false;
            buttonBin.interactable = false;

            if(!isRobotShutDown){
                isRobotShutDown = true;
                soundManager.RobotTyping(false);
                soundManager.RobotPower();
                robotManager.RobotPowerless(true);
            }

            rechargeTimer += Time.deltaTime;
            if ((int)rechargeTimer == secondsToRecharge)
            {
                isRobotShutDown = false;
                robotManager.RobotPowerless(false);
                robotBatteryMask.GetComponent<Transform>().localScale = new Vector3(robotBatteryMask.GetComponent<Transform>().localScale.x,
                                                                                    0,
                                                                                    robotBatteryMask.GetComponent<Transform>().localScale.z);
                power = 100;
                powerTimer = 0;
                rechargeTimer = 0;
                soundManager.RobotTyping(true);
                isPower = true;
                buttonThunder.interactable = true;
                buttonBattery.interactable = true;
                buttonBin.interactable = true;
            }
        }

        if ((int)power % 20 == 0 && power != 100 && !isBatteryDescending)
        {
            isBatteryDescending = true;
            StartCoroutine(DescendingBattery());
        }
    }

    IEnumerator DescendingBattery(){
        robotBatteryMask.GetComponent<Transform>().localScale = new Vector3(robotBatteryMask.GetComponent<Transform>().localScale.x,
                                                                            robotBatteryMask.GetComponent<Transform>().localScale.y + 0.72f,
                                                                            robotBatteryMask.GetComponent<Transform>().localScale.z);
        yield return new WaitForSeconds(periodPowerDecrase);
        isBatteryDescending = false;
    }

    void SpaceUpdate(){
        UpdateSpaceUI();
        spaceText.text = "space: " + space;
        if(space == 100){
            space = Mathf.Clamp(space - 20, 0, 100);
            DisplayMessage(spaceText.gameObject, "-20", Color.green);

            testSubjects[positionActualSubject] = 0;
            cellsSubjectList[positionActualSubject].color = redColor; //added to cell color
            subjectManager.SubjectBurn();
            positionActualSubject++;
            StartCoroutine(BurnedSoundAfterWhile());

            robotManager.RobotInterrupted();
            StartCoroutine(DisplayScreenAlerts());

            if (positionActualSubject > testSubjects.Length)
            {
                EndRound();
            }
            else
            {
                NextSubject(true);
            }
        }
    }
    IEnumerator BurnedSoundAfterWhile()
    {
        yield return new WaitForSeconds(1.5f);
        soundManager.SubjectScream();
    }
    IEnumerator DisplayScreenAlerts(){
        soundManager.AlertSound();
        pauseMechanichs = true;
        screenAlert.SetActive(true);
        soundManager.RobotTyping(false);
        yield return new WaitForSeconds(2.5f);
        soundManager.RobotTyping(true);
        screenAlert.SetActive(false);
        pauseMechanichs = false;
    }

    void SetUpMiMicSystem(){
        mimicChallengeTimer = 0;
        KeyCode dataKey1, dataKey2, dataKey3;
        KeyCode[] listOfKeys = Constants.listOfKeyCodes;

        dataKey1 = listOfKeys[(int)Random.Range(0, listOfKeys.Length)];
        dataKey2 = listOfKeys[(int)Random.Range(0, listOfKeys.Length)];
        dataKey3 = listOfKeys[(int)Random.Range(0, listOfKeys.Length)];

        actualKeyChallenge[0] = dataKey1;
        actualKeyChallenge[1] = dataKey2;
        actualKeyChallenge[2] = dataKey3;

        key1.text = dataKey1.ToString();
        key2.text = dataKey2.ToString();
        key3.text = dataKey3.ToString();

        key1.color = Color.white;
        key2.color = Color.white;
        key3.color = Color.white;

        boxLetter1.color = Color.white;
        boxLetter2.color = Color.white;
        boxLetter3.color = Color.white;
    }

    void MimicSystemUpdate(){
        if (!mimicPaused)
        {
            mimicChallengeTimer += Time.deltaTime;
            if ((int)mimicChallengeTimer == secondsPerMimicChallenge)
            {
                RunOutOfTime();
                //To do 
                //wait a few seconds and display mistake animation in screen.
                SetUpMiMicSystem();

            }

            MimicSytemFeedback();
        }
    }

    void RunOutOfTime()
    {
        key1.color = Color.red;
        key2.color = Color.red;
        key3.color = Color.red;

        mimicPosition = 0;

        if (life > 0)
            lifeMask.GetComponent<Transform>().localScale = new Vector3(lifeMask.GetComponent<Transform>().localScale.x - 2,
                                           timeMask.GetComponent<Transform>().localScale.y,
                                           timeMask.GetComponent<Transform>().localScale.z);

        life = Mathf.Clamp(life - 10, 0, 100);
        DisplayMessage(lifeText.gameObject, "-10", Color.red);
    }


    void MimicSytemFeedback(){
        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) 
            && !Input.GetKeyDown(KeyCode.Mouse0) && !Input.GetKeyDown(KeyCode.Mouse1) && !Input.GetKeyDown(KeyCode.Mouse2)
            && !Input.GetKeyDown(KeyCode.Mouse3) && !Input.GetKeyDown(KeyCode.Mouse4) && !Input.GetKeyDown(KeyCode.Mouse5)
            && !Input.GetKeyDown(KeyCode.Mouse6) && isPower)
        {
            KeyCode keyPressed = KeyCode.Space; //I cant put null
            foreach (KeyCode kcode in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(kcode))
                    keyPressed = kcode;
            }

            if (keyPressed == actualKeyChallenge[mimicPosition]){
                switch(mimicPosition){
                    case 0:
                        key1.color = greenColor;
                        boxLetter1.color = greenColor;
                        mimicPosition++;
                        break;
                    case 1:
                        key2.color = greenColor;
                        boxLetter2.color = greenColor;
                        mimicPosition++;
                        break;
                    case 2:
                        key3.color = greenColor;
                        mimicPosition = 0;
                        boxLetter3.color = greenColor;
                        //To do 
                        //wait a few seconds and display all correct animation in screen.
                        life = Mathf.Clamp(life + 10, 0, 100);
                        lifeMask.GetComponent<Transform>().localScale = new Vector3(lifeMask.GetComponent<Transform>().localScale.x + 2,
                                                                       timeMask.GetComponent<Transform>().localScale.y,
                                                                       timeMask.GetComponent<Transform>().localScale.z);
                        StartCoroutine(ChangeColorLines(greenColor));
                        BonusChance();
                        DisplayMessage(lifeText.gameObject, "+10", Color.green);
                        break;
                }
            }else{
                key1.color = Color.red;
                key2.color = Color.red;
                key3.color = Color.red;

                mimicPosition = 0;

                life = Mathf.Clamp(life-10, 0, 100);
                if (life > 0)
                {
                    lifeMask.GetComponent<Transform>().localScale = new Vector3(lifeMask.GetComponent<Transform>().localScale.x - 2,
                                                timeMask.GetComponent<Transform>().localScale.y,
                                                timeMask.GetComponent<Transform>().localScale.z);
                }else if(life==0){
                    lifeMask.GetComponent<Transform>().localScale = new Vector3(2,
                                                timeMask.GetComponent<Transform>().localScale.y,
                                                timeMask.GetComponent<Transform>().localScale.z);
                }
                DisplayMessage(lifeText.gameObject, "-10", Color.red);

                StartCoroutine(ChangeColorLines(redColor));
            }
        }
    }

    IEnumerator ChangeColorLines(Color color)
    {
        linesScreen.color = color;
        key1.color = color;
        key2.color = color;
        key3.color = color;
        boxLetter1.color = color;
        boxLetter2.color = color;
        boxLetter3.color = color;
        //lifeMask.GetComponent<SpriteRenderer>().color = color;
        lifeCells.color = color;
        yield return new WaitForSeconds(0.25f);
        linesScreen.color = Color.white;
        key1.color = Color.white;
        key2.color = Color.white;
        key3.color = Color.white;
        lifeMask.GetComponent<SpriteRenderer>().color = Color.white;
        lifeCells.color = Color.white;
        SetUpMiMicSystem();

    }

    void NextSubject(bool burned){
        subjectName.text = "SUBJECT " + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "" + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "-" + (char)dicctionary[Random.Range(0, dicctionary.Length)]
                    + "" + (char)dicctionary[Random.Range(0, dicctionary.Length)];
        if(!burned)
            subjectManager.SubjectOut();
        time = 32;
        life = 0;
        lifeMask.GetComponent<Transform>().localScale = new Vector3(2,
                timeMask.GetComponent<Transform>().localScale.y,
                timeMask.GetComponent<Transform>().localScale.z);
        lifeTimer = 0;
        timeMask.GetComponent<Transform>().localScale = new Vector3(8.0f,
                                                                timeMask.GetComponent<Transform>().localScale.y,
                                                                timeMask.GetComponent<Transform>().localScale.z);
    }

    void BonusChance(){
        int probabilityThunder = 5;
        int probabilityBattery = 12;
        int probabilityBin = 10;

        int randomNumber = Random.Range(0, 100);

        if(randomNumber < probabilityThunder){
            //gain 1 save
            hasOneThunder = true;
            //buttonThunder.GetComponent<Transform>().GetChild(0).GetComponent<Text>().text = "x1 Thunder";
            //create message
            DisplayMessage(buttonThunder.gameObject, "+1", Color.green);
        }else if(randomNumber > probabilityThunder && randomNumber < (probabilityBattery + probabilityThunder)){
            //gain 1 battery
            hasOneBattery = true;
            //create message
            DisplayMessage(buttonBattery.gameObject, "+1", Color.green);
        }else if(randomNumber > (probabilityBattery + probabilityThunder) && randomNumber < (probabilityBattery + probabilityThunder + probabilityBin)){
            //gain 1 bin
            hasOneBin = true;
            //create message
            DisplayMessage(buttonBin.gameObject, "+1", Color.green);
        }
    }



    public void ThunderButtonIntercation()
    {
        if(hasOneThunder){
            hasOneThunder = false;
            int randomNumber = Random.Range(0, 100);
            if(randomNumber < 70){
                life = Mathf.Clamp(life+100, 0, 100);
                lifeMask.GetComponent<Transform>().localScale = new Vector3(22,
                                timeMask.GetComponent<Transform>().localScale.y,
                                timeMask.GetComponent<Transform>().localScale.z);
                DisplayMessage(lifeText.gameObject, "+100", Color.green);
            }else{
                //subject die for an overtension
                DisplayMessage(lifeText.gameObject, "-100", Color.red);
                testSubjects[positionActualSubject] = 0;
                cellsSubjectList[positionActualSubject].color = redColor; //added to cell color
                positionActualSubject++;
                subjectManager.SubjectBurn();
                soundManager.SubjectScream();

                if (positionActualSubject > testSubjects.Length)
                {
                    EndRound();
                }
                else
                {
                    NextSubject(true);
                }
            }
        }
    }

    public void BatteryButtonIntercation()
    {
        if(hasOneBattery){
            hasOneBattery = false;
            if(power < 100)
                robotBatteryMask.GetComponent<Transform>().localScale = new Vector3(robotBatteryMask.GetComponent<Transform>().localScale.x,
                                                                robotBatteryMask.GetComponent<Transform>().localScale.y - 0.72f,
                                                                robotBatteryMask.GetComponent<Transform>().localScale.z);
            power = Mathf.Clamp(power + 20, 0, 100);

            DisplayMessage(powerText.gameObject, "+20", Color.green);
        }
    }

    void EndRound(){
        pauseMechanichs = true;
    }

    public void BinButtonIntercation()
    {
        if (hasOneBin)
        {
            hasOneBin = false;
            space = Mathf.Clamp(space - 10, 0, 100);
            DisplayMessage(spaceText.gameObject, "-10", Color.green);
        }
    }

    void DisplayMessage(GameObject location, string text, Color color){
        GameObject message = Instantiate(feedbackMessagePrefab, location.transform.position, Quaternion.identity);
        message.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshProUGUI>().text = text;
        message.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshProUGUI>().color = color;
        message.GetComponent<Transform>().parent = canvas.GetComponent<Transform>();
        message.GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
        message.GetComponent<Transform>().localPosition = location.transform.localPosition;
        Destroy(message, 2);
    }

    void UpdateButtons(){
        if(!hasOneThunder){
            buttonThunder.interactable = false;
            buttonThunder.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = iconDeactivated;
        }else if(isPower){
            buttonThunder.interactable = true;
            buttonThunder.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = Color.white;
        }

        if(!hasOneBattery){
            buttonBattery.interactable = false;
            buttonBattery.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = iconDeactivated;
        }else if(isPower){
            buttonBattery.interactable = true;
            buttonBattery.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = Color.white;
        }

        if(!hasOneBin){
            buttonBin.interactable = false;
            buttonBin.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = iconDeactivated;
        }else if(isPower){
            buttonBin.interactable = true;
            buttonBin.GetComponent<Transform>().GetChild(1).GetComponent<Image>().color = Color.white;
        }
    }

    void OpenReport(){
        canvasWindowsReport.SetActive(true);
        pauseMechanichs = true;
        int failedSubjects = 0;
        for (int i = 0; i < testSubjects.Length; i++)
        {
            if(testSubjects[i] == 1){
                reportCellsSubjectResult[i].color = greenColor;
            }else{
                failedSubjects++;
                reportCellsSubjectResult[i].color = redColor;
            }
        }

        switch (failedSubjects)
        {
            case 0:
                calification.text = "A+";
                calification.color = greenColor;
                break;
            case 1:
                calification.text = "A";
                calification.color = greenColor;
                break;
            case 2:
                calification.text = "B+";
                calification.color = greenColor;
                break;
            case 3:
                calification.text = "B";
                calification.color = greenColor;
                break;
            case 4:
                calification.text = "B-";
                calification.color = greenColor;
                break;
            case 5:
                calification.text = "C+";
                calification.color = greenColor;
                break;
            case 6:
                calification.text = "C";
                calification.color = greenColor;
                break;
            case 7:
                calification.text = "C-";
                calification.color = redColor;
                break;
            case 8:
                calification.text = "D+";
                calification.color = redColor;
                break;
            case 9:
                calification.text = "D";
                calification.color = redColor;
                break;
            case 10:
                calification.text = "F";
                calification.color = redColor;
                break;
            default:
                break;
        }
    }

    public void RestartGame(){
        
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);

    }

    public void CloseWindowPause(){
        Time.timeScale = 1;
        pauseCanvas.SetActive(false);
    }

    public void QuitGame(){
        Application.Quit();
    }

    public void OpenTutorial(){
        pauseMechanichs = true;
        canvasTutorial.SetActive(true);
    }

    public void CloseTutorial()
    {
        pauseMechanichs = false;
        canvasTutorial.SetActive(false);
        Constants.isNewPlayer = false;
    }

}
