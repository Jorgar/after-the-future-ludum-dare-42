﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LDRobotManager : MonoBehaviour {

    public Animator animator;

	// Use this for initialization
    public void RobotPowerless(bool isIn){
        if(isIn){
            animator.SetTrigger("robot_powerless_in");
        }else{
            animator.SetTrigger("robot_powerless_out");
        }
    }

    public void RobotInterrupted(){
        animator.SetTrigger("robot_interrupted");
    }
}
